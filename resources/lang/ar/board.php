<?php

return [

	'english_lang'   => 'اللغه الانجليزيه',
	'arabic_lang'    => 'اللغه العربيه',
	'board'          => 'لوحه التحكم',
	'home'           => 'الرئيسيه',
	'logout'         => 'تسجيل الخروج',
	'update_success' => 'تم تعديل البيانات بنجاح',
	'admins'         => 'المشرفين',
	'trips'          => 'طلبات التوصيل',
	'drivers'        => 'السائقين',
	'markets'        => 'المتاجر',
	'shortcuts'      => 'الروابط المختصره',
	'customers_count'    => ' العملاء',
	'governorates_count' => ' المحافظات',
	'cities_count'       => ' المدن',
	'markets_count'      => ' المتاجر',
	'admins_count'       => ' المشرفين',
	'drivers_count'      => ' السائقين',
	'accountants_count'  => ' المحاسبين',
	'call_center_count'  => ' خدمه العملاء',
	'trips_count'      => 'اجمالى الرحلات' , 
	'markets_are_about_to_be_expired' => ' المتاجر التى قارب انتهاء اشتراكها' , 
	'expirated_markets_subscription' => 'المتاجر المنتهى اشتراكها' , 
	'most_deliverd_governorates' => 'المحافظات الاكثر توصيلا' , 
	'most_deliverd_cities' => 'المدن الاكثر توصيلا' , 
	'track_drivers_on_map' => 'تتبع السائقين' , 
	'settings' => 'اعدادات' , 
	'edit_settings' => 'تعديل الاعدادات ' , 
		'yes' => 'نعم' , 
	'no' => 'لا' ,
	'automatic_orders_distribution' => 'توزيع الطلبات بشكل الى ' ,
	'settings_updated' => 'تم تعديل الاعدادات بنجاح' , 






];

?>