<?php 

return [

	'bills' => 'الفواتير'  , 
	'driver' => 'السائق' , 
	'admin' => 'المدير'  , 
	'created_at' => 'تاريخ الاضافه'  , 
	'status' => 'الحاله' , 
	'price' => 'المبلغ' , 
	'comment' => 'التعليق' , 
	'type' => 'نوع الفاتوره' , 
	'show_all_bills' => 'عرض كافه الفواتير' , 
	'bill' => 'الفاتوره' , 
	'settings' => 'خصائص' , 
	'waiting' =>  'قيد المراجعه' , 
	'accepted' => 'تم القبول' , 
	'refused' => 'تم الرفض'  , 
	'show_bill_details' => 'عرض تفاصيل الفاتوره' , 
	'number' => 'رقم الفاتوره' , 
		'updated_at' => 'تاريخ اخر تعديل' , 
	'bill_image' => 'صوره الفاتوره' , 
	'refuse' => 'رفض'  , 
	'accept' => 'موافقه' , 
	'all_drivers' => 'جميع السائقين' , 
	'print' => 'طباعه الفاتوره' , 
	'all_status' => 'كافه الحالات' , 

];
 ?>