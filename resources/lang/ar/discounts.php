<?php 

return [

	'discounts' => 'الخصومات' , 
	'add_new_discount' => 'إضافه خصم جديد',
	'discount' => 'خصم' , 
	'active' => 'فعال' ,
	'inactive' => 'غير فعال' , 
	'add' => 'إضافه' , 
	'search_discounts' => 'البحث داخل الخصومات' , 
	'show_all_discounts' => 'عرض كافه الخصومات',
	'activation' => 'التفعيل'  , 
	'discount' => 'خصم' , 
	'settings' => 'خصائص' , 
	'edit' => 'تعديل' , 
	'edit_discount_details' => 'تعديل بيانات الخصم' , 
	'updating_error' => 'خطا حاول مره اخرى' , 
	'updating_success' => 'تم التعديل بنجاح' , 
	'discount_details' => 'بيانات الخصم' , 
	'created_at' => 'تاريخ الاضافه' , 
	'added_by' => 'تم الإضافه بواسطه'  , 
	'delete_discount' => 'حذف الخصم' , 
	'deleted_success' => 'تم حذف الخصم بنجاح',
	'picture' => 'الصوره الشخصيه' , 
	'back' => 'تراجع' ,
	'deleted_success' => 'تم الحذف بنجاح' , 
	'adding_success' => 'تم إضافه الخصم بنجاح',
	'adding_error' => 'خطا حاول مره اخرى' , 
	'name_ar' => 'الاسم بالعربيه',
	'name_en' => 'الاسم بالانجليزيه',
	'country' => 'الدوله' , 
	'search' => 'بحث' , 
	'discount_delivery_prices' => 'سعر التوصيل للمحافظات' ,
	'deliver_to_discount' => 'توصيل الى خصم' , 
	'all_discounts_are_done' => 'تم وضع اسعار لجميع الخصومات' , 
	'number_cities' => 'عدد المدن التباعه للخصم' , 
	'delivery_prices' => 'اسعار التوصيل' , 
	'delivery_price' => 'سعر التوصيل' , 
	'delete_delivery_price' => 'حذذف سعر التوصيل' , 
	'edit_delivery_price_details' => 'تعديل سعر التوصيل اللى الخصم' , 
	'editing_delivery_price_success' => 'تم تعديل سعر التوصيل بنجاح',
	'no_delivery_prices_yet' => 'لم يتم إضافه اى اسعار توصيل خاصه بهذا الخصم للمحافظات الاخر' , 
	'add_delivery_prices_to_discount' => 'إضافه سعر التوصيل الى الخصومات الاخرى' , 
	'delivery_prices_add_successfully' => 'تم إضافه اسعار التوصيل الى الخصومات الاخر بنجاح' , 
	'from_discount' => 'من خصم'  , 
	'to_discount' => 'الى خصم'  , 
	'cities_count' => 'عدد المدن' , 
	'title' => 'العنوان' , 
	'specific_market' => 'تطبيق الخصم على متاجر محدده' , 
	'specific_area' => 'تطبيق الخصم على اماكن محدده' , 
	'percentage' => 'نسبه الخصم',
	'markets_only' => 'المتاجر فقط' , 
	'customers_only' => ' العملاء فقط' , 
		'markets' => 'المتاجر ' , 
	'customers' => ' العملاء ' , 
	'all' => 'الجميع' ,
	'discount_for' => 'تطبيق هذا الخصم على ' , 
	'start_date' => 'تاريخ البدىء' , 
	'end_date' => ' تاريخ الانتهاء ' , 
	'allowed_used_count' => 'عدد مرات الاستخدام' , 
	'governorates' => 'المحافظات'  , 









	







];

?>