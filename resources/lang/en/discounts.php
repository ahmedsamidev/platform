<?php 

return [


	'discounts' => 'discounts' , 
	'add_new_discount' => 'Add New discount' , 

	'discount' => 'discount' , 
	'active' => 'Active' ,
	'inactive' => 'In Active' , 
	'add' => 'Add'  , 
	'search_discounts' => 'Search discounts' , 
	'show_all_discounts' => 'Show All discounts',
	'activation' => 'Activation' , 
	'edit' => 'Edit' , 
	'edit_discount_details' => 'Edit discount details' , 
	'updating_success' => 'Data updated successfully' , 
	'updating_error' => 'Error Try again' , 
	'discount_details' => 'discount Details' , 
	'created_at' => 'Created At' , 
	'added_by' => 'Added By' , 
	'delete_discount' => 'Delete discount' , 
	'deleted_success' => 'discount deleted  successfully' , 
	'back' => 'Back' ,
	'adding_success' => 'discount added successfully',
	'adding_error' => 'Error Try again' , 
	'country' => 'Country' , 
	'search' => 'Search' , 
	'discount_delivery_prices' => 'discount delivery prices'  , 
	'deliver_to_discount' => 'deliver to discount' , 
	'all_discounts_are_done' => 'All discounts are priced'  , 
	'number_cities' => 'number of cities in this discount'  , 
	'delivery_prices' => 'delivery prices' , 
	'delivery_price' => 'delivery price' , 
	'delete_delivery_price' => 'delete delivery price' , 
	'edit_delivery_price_details' => 'edit delivery price details' , 
	'editing_delivery_price_success' => 'editing delivery price success',
	'no_delivery_prices_yet' => 'no delivery prices yet made to this discount' , 
	'add_delivery_prices_to_discount' => 'add delivery prices to discount' ,
	'delivery_prices_add_successfully' => 'delivery prices to other discounts added successfully' , 
	'from_discount' => 'From discount'  , 
	'to_discount' => 'To discount'  , 
	'cities_count' => 'cities Count'  , 
	'title' => 'Title' , 
	'specific_market' => 'Apply this discount on specified markets' , 
	'specific_area' => 'Apply this discount on specified area' , 
	'percentage' => 'percentage', 
	'discount_for' => 'discount for' , 
	'markets' => 'markets' , 
	'customers' => 'customers' , 
	'governorates' => 'Governorates' , 
	'markets_only' => 'markets only' , 
	'customers_only' => 'customers only' , 
	'all' => 'All' , 
	'start_date' => 'start date' , 
	'end_date' => 'end date' , 
	'allowed_used_count' => 'allowed_used_count' , 


	
];

 ?>