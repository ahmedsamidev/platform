@extends('board.layout.master')

@section('title')
@lang('board.home')
@endsection


@section('header')
<div class="page-header">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-right6 mr-2"></i> @lang('profile.profile') </h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="header-elements d-none py-0 mb-3 mb-md-0">
			<div class="breadcrumb">
				<a href="{{ route('board.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>
				<span class="breadcrumb-item active"> @lang('profile.profile') </span>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<!-- Inner container -->
<div class="d-md-flex align-items-md-start">
	<!-- Right content -->
	<div class="tab-content w-100">
		@include('board.layout.messages')
		<div class="tab-pane fade active show" >
			<div class="card">
				<div class="card-header header-elements-inline">
					<h5 class="card-title"> @lang('board.edit_settings') </h5>
					<div class="header-elements">
						<div class="list-icons">
							<a class="list-icons-item" data-action="collapse"></a>
							<a class="list-icons-item" data-action="reload"></a>
							<a class="list-icons-item" data-action="remove"></a>
						</div>
					</div>
				</div>			
				<form action="{{ route('board.settings.update') }}"  method="POST">
					<div class="card-body">
						@csrf
						@method('PATCH')
						<table class="table table-bordered table-hover">
							<tbody>
								<tr>
									<th> @lang('board.automatic_orders_distribution') </th>
									<td>
										<div class="form-check form-check-switchery form-check-switchery-double">
											<label class="form-check-label">
												@lang('board.yes')
												<input type="checkbox" name="automatic_orders_distribution" value="true" class="form-check-input-switchery"  data-fouc {{ $settings->automatic_orders_distribution == 1 ? 'checked' : '' }} >
												@lang('board.no')
											</label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="card-footer">
						<div class="text-right">
							<button type="submit" class="btn btn-warning"> @lang('profile.edit') </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('styles')
@endsection
@section('scripts')
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/demo_pages/user_pages_profile_tabbed.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
@endsection