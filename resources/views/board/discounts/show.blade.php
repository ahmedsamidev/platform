@php
$lang = session()->get('locale');
@endphp
@extends('board.layout.master')
@section('title')
@lang('discounts.discount_details')
@endsection


@section('header')
<div class="page-header ">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-right6 mr-2"></i> @lang('discounts.discounts') </h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="header-elements d-none py-0 mb-3 mb-md-0">
			<div class="breadcrumb">
				<a href="{{ route('board.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>
				<a href="{{ route('discounts.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('discounts.discounts') </a>
				<span class="breadcrumb-item active"> @lang('discounts.discount_details') </span>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')

<div class="row">

	<div class="col-md-12 mb-3">
		<div class="header-elements ">
			<div class="float-right">
				<a href="{{ route('discounts.create') }}" class="btn btn-primary ml-1"> <i class="icon-user-plus"></i> @lang('discounts.add_new_discount')  </a>
				<a href="{{ route('discounts.edit'  , ['discount' => $discount->id ] ) }}" class="btn btn-warning ml-1"> <i class="icon-pencil5"></i> @lang('discounts.edit_discount_details')  </a>
				<form action="{{ route('discounts.destroy'  , ['discount' => $discount->id] ) }}" method="POST" class="float-right ml-1">
					@csrf
					@method('DELETE')
					<button href="#" class="btn btn-danger "> <i class="icon-trash"></i> @lang('discounts.delete_discount') </button>
				</form>
			</div>
		</div>
	</div>




	<div class="col-md-12">

		@include('board.layout.messages')
		<!-- Account settings -->
		<div class="card">
			<div class="card-header bg-dark header-elements-inline">
				<h5 class="card-title"> @lang('discounts.discount_details') {{ $discount->username }} </h5>
				<div class="header-elements">
					<div class="list-icons">
						<a class="list-icons-item" data-action="collapse"></a>
						<a class="list-icons-item" data-action="reload"></a>
						<a class="list-icons-item" data-action="remove"></a>
					</div>
				</div>
			</div>

			<div class="card-body">
				<table class="table   table-xs border-top-0 my-2">
					<tbody>
						<tr>
							<th class="font-weight-bold text-dark">@lang('discounts.title')</th>
							<td class="text-left"> {{ $discount->title }} </td>
						</tr>
						<tr>
							<th class="font-weight-bold text-dark">@lang('discounts.percentage')</th>
							<td class="text-left"> {{ $discount->discount_percentage }} %</td>
						</tr>

						<tr>
							<th class="font-weight-bold text-dark">@lang('discounts.allowed_used_count')</th>
							<td class="text-left"> {{ $discount->allowed_used_count }} </td>
						</tr>


						<tr>
							<th class="font-weight-bold text-dark">@lang('discounts.start_date')</th>
							<td class="text-left"> {{ $discount->starts_at->toDateTimeString() }} - <span class="text-muted">  {{ $discount->starts_at->diffForHumans() }} </span> </td>
						</tr>

						<tr>
							<th class="font-weight-bold text-dark">@lang('discounts.end_date')</th>
							<td class="text-left"> {{ $discount->ends_at->toDateTimeString()}} - <span class="text-muted">  {{ $discount->ends_at->diffForHumans() }} </span> </td>
						</tr>


						

						<tr>
							<th class="font-weight-bold text-dark"> @lang('discounts.activation') </th>
							<td class="text-left">	
								@switch($discount->active)
								@case(1)
								<label  class="badge badge-success" > @lang('discounts.active') </label>
								@break
								@case(0)
								<label  class="badge badge-secondary" > @lang('discounts.inactive') </label>
								@break
								@endswitch
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold text-dark"> @lang('discounts.created_at') </td>
							<td class="text-left"> {{ $discount->created_at->toFormattedDateString() }} - {{ $discount->created_at->diffForHumans() }} </td>
						</tr>

						<tr>
							<td class="font-weight-bold text-dark"> @lang('discounts.added_by') </td>
							<td class="text-left font-weight-bold"> <a href="{{ route('admins.show'  , ['admin' => $discount->admin_id] ) }}"> {{ optional($discount->admin)->username }} </a> </td>
						</tr>
						<tr>
							<td class="font-weight-bold text-dark"> @lang('discounts.governorates') </td>
							<td class="text-left font-weight-bold"> 
								<ul>
									@foreach ($discount->governorates as $governorate)
										<li> {{ optional($governorate->governorate)['name_'.$lang] }} </li>
									@endforeach
								</ul>
							 </td>
						</tr>
						<tr>
							<td class="font-weight-bold text-dark"> @lang('discounts.markets') </td>
							<td class="text-left font-weight-bold"> 
								<ul>
									@foreach ($discount->markets as $market)
										<li> {{ optional($market->market)['name'] }} </li>
									@endforeach
								</ul>
							 </td>
						</tr>
					

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection


@section('styles')

@endsection

@section('scripts')

@endsection