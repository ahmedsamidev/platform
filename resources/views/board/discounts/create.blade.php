@php
$lang = session()->get('locale');
@endphp
@extends('board.layout.master')
@section('title')
@lang('profile.profile')
@endsection


@section('header')
<div class="page-header">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-right6 mr-2"></i> @lang('discounts.discounts') </h4>
			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>
		<div class="header-elements d-none py-0 mb-3 mb-md-0">
			<div class="breadcrumb">
				<a href="{{ route('board.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>
				<a href="{{ route('discounts.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('discounts.discounts') </a>
				<span class="breadcrumb-item active"> @lang('discounts.add_new_discount') </span>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">

		@include('board.layout.messages')
		<!-- Account settings -->
		<div class="card">
			<div class="card-header bg-dark header-elements-inline">
				<h5 class="card-title"> @lang('discounts.add_new_discount') </h5>
			</div>
			<form action="{{ route('discounts.store') }}"  method="POST">
				<div class="card-body">
					@csrf
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<th> @lang('discounts.title') </th>
								<td>
									<input type="text" name="title" id="input" class="form-control" value="{{ old('title') }}" >
								</td>
							</tr>
							<tr>
								<th> @lang('discounts.percentage') </th>
								<td>
									<input type="text" name="percentage"  class="form-control" value="{{ old('percentage') }}" >
								</td>
							</tr>
							<tr>
								<th> @lang('discounts.allowed_used_count') </th>
								<td>
									<input type="text" name="allowed_used_count"  class="form-control" value="{{ old('allowed_used_count') }}" >
								</td>
							</tr>
							<tr>
								<th> @lang('discounts.start_date') </th>
								<td>
									<div class="row">
										<div class="col-md-9">
											<input type="text" name="start_date" id="daterange-single"  class="form-control" value="{{ old('start_date') }}" >
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<span class="input-group-prepend">
													<span class="input-group-text"><i class="icon-alarm"></i></span>
												</span>
												<input type="text" class="form-control pickatime2" name="start_time" value="{{ old('start_time') }}" >
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th> @lang('discounts.end_date') </th>
								<td>
									<div class="row">
										<div class="col-md-9">
											<input type="text" name="end_date" id="daterange-single2" class="form-control" value="{{ old('end_date') }}" >
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<span class="input-group-prepend">
													<span class="input-group-text"><i class="icon-alarm"></i></span>
												</span>
												<input type="text" class="form-control pickatime" name="end_time" value="{{ old('end_time') }}" >
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr >
								<th> @lang('discounts.discount_for') </th>
								<td>
									<div class="form-check form-check-switchery form-check-switchery-double">
										<select name="discount_for"  class="form-control" >
											<option value="3"> @lang('discounts.all') </option>
											<option value="1"> @lang('discounts.markets_only') </option>
											<option value="2"> @lang('discounts.customers_only') </option>
										</select>
									</div>
								</td>
							</tr>

							<tr class="apply_on_markets" >
								<th> @lang('discounts.specific_market') </th>
								<td>
									<div class="form-check form-check-switchery form-check-switchery-double">
										<label class="form-check-label">
											@lang('board.yes')
											<input type="checkbox" name="specific_market" value="true" class="form-check-input-switchery"  data-fouc  >
											@lang('board.no')
										</label>
									</div>
								</td>
							</tr>
							<tr class="apply_on_area">
								<th> @lang('discounts.specific_area') </th>
								<td>
									<div class="form-check form-check-switchery form-check-switchery-double">
										<label class="form-check-label">
											@lang('board.yes')
											<input type="checkbox" name="specific_area" value="true" class="form-check-input-switchery"  data-fouc  >
											@lang('board.no')
										</label>
									</div>
								</td>
							</tr>

							<tr class="markets_tr" >
								<th> @lang('discounts.markets') </th>
								<td>
									<select name="markets[]"  class="form-control market_id" multiple="multiple">
									</select>
								</td>
							</tr>

							<tr class="customers_tr" >
								<th> @lang('discounts.customers') </th>
								<td>
									<select name="customers[]" id="input" class="form-control"  multiple="multiple">
										<option value=""></option>
										@foreach ($customers as $customer)
										<option value="{{ $customer->id }}"> {{ $customer->name }} </option>
										@endforeach
									</select>
								</td>
							</tr>
							<tr class="governorates_tr" >
								<th> @lang('discounts.governorates') </th>
								<td>
									<ul>
										@foreach ($governorates as $governorate)
										<div class="form-check">
											<label class="form-check-label">
												<input type="checkbox" name="governorates[]" value="{{  $governorate->id  }}" class="form-check-input-styled" data-fouc>
												{{ $governorate['name_'.$lang] }}
											</label>
										</div>
										@endforeach
									</ul>
								</td>
							</tr>				
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<div class="text-right">
						<button type="submit" class="btn btn-warning"> @lang('discounts.add') </button>
					</div>
				</div>
			</form>
		</div>
		<!-- /account settings -->
	</div>
</div>

@endsection


@section('styles')

@endsection

@section('scripts')
{{-- <script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script> --}}
<script src="{{ asset('board_assets/global_assets/js/demo_pages/user_pages_profile_tabbed.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/pickadate/legacy.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script>
	$(function() {


		$('#daterange-single').daterangepicker({ 
			singleDatePicker: true,
			// format: '%y-%m-%d',
			opens: 'left',
			locale: {
				direction: 'rtl',
				format: 'Y-M-D',
			}
		});

		$('#daterange-single2').daterangepicker({ 
			singleDatePicker: true,
			// format: '%y-%m-%d',
			opens: 'left',
			locale: {
				direction: 'rtl',
				format: 'Y-M-D',
			}
		});

		$('.pickatime').pickatime();
		$('.pickatime2').pickatime();



		$('tr.apply_on_markets').hide();
		$('tr.markets_tr').hide();
		$('tr.customers_tr').hide();
		$('tr.governorates_tr').hide();


		$('.market_id').select2({
			placeholder: "اختر المتجر",
			minimumInputLength:3,
			ajax: {
				url: '/Board/ajax/search_markets',
				dataType: 'json',
				type: 'GET' ,
				data: function (params) {
					var queryParameters = {
						q: params.term ,
					}
					return queryParameters;
				},
				delay: 500,
				processResults: function (data) {
					return {
						results:  $.map(data, function (item) {
							return {
								text: item.name  ,
								id: item.id
							}
						})
					};
				},
				cache: true
			}
		});








		$('select[name="discount_for"]').on('change',  function(event) {
			event.preventDefault();

			var selected_value = $(this).val();
			if (selected_value == 1) {
				$('tr.apply_on_markets').show();
			}

			if (selected_value == 3) {
				$('tr.apply_on_markets').hide();
				$('tr.customers_tr').hide();
				$('tr.markets_tr').hide();
			}
			if (selected_value == 2) {
				$('tr.customers_tr').show();
				$('tr.markets_tr').hide();
				$('tr.apply_on_markets').hide();
			}
		});

		$('input[name="specific_market"]').on('change', function(event) {
			event.preventDefault();

			if ($(this).is(':checked')) {
				$('tr.markets_tr').show();
			} else {
				$('tr.markets_tr').hide();

			}
		});


		$('input[name="specific_area"]').on('change', function(event) {
			event.preventDefault();
			if ($(this).is(':checked')) {
				$('tr.governorates_tr').show();
			} else {
				$('tr.governorates_tr').hide();

			}
		});

	});
</script>
@endsection