@php
  header("Content-type: text/css; charset: UTF-8");
$lang = session()->get('locale');
@endphp
<!DOCTYPE html>
<html lang="ar">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>PDF</title>
  <style>


    .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }

    a {
      color: #0087C3;
      text-decoration: none;
    }

    body {
      direction: rtl;
      position: relative;
      width: 100%;  
      height: 29.7cm; 
      margin: 0 auto; 
      color: #555555;
      background: #FFFFFF; 

      font-family: 'Almarai' , sans-serif;

      font-size: 12px; 
      font-weight: bold;
    }

    header {
      padding: 10px 0;
      margin-bottom: 20px;
      border-bottom: 1px solid #AAAAAA;
    }


    #company {
      float: right;
      text-align: right;
    }


    #details {
      margin-bottom: 50px;
    }

    #client {
      padding-left: 6px;
      border-left: 6px solid #0087C3;
      float: left;
    }

    #client .to {
      color: #777777;
    }

    h2.name {
      font-size: 1.4em;
      font-weight: normal;
      margin: 0;
    }

    #invoice {
      float: right;
      text-align: right;
    }

    #invoice h1 {
      color: #0087C3;
      font-size: 2.4em;
      line-height: 1em;
      font-weight: normal;
      margin: 0  0 10px 0;
    }

    #invoice .date {
      font-size: 1.1em;
      color: #777777;
    }


    #thanks{
      font-size: 2em;
      margin-bottom: 50px;
    }

    #notices{
      padding-left: 6px;
      border-left: 6px solid #0087C3;  
    }

    #notices .notice {
      font-size: 1.2em;
    }

    footer {
      color: #777777;
      width: 100%;
      height: 30px;
      position: absolute;
      bottom: 0;
      border-top: 1px solid #AAAAAA;
      padding: 8px 0;
      text-align: center;
    }

    table {
      text-align: center;
    }


    table thead tr th {
      background-color: #3b8668;
      color: white;
    }


  </style>
</head>
<body dir="rtl">

  <main>

    <table border="1" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th> @lang('bills.bill_number') </th>
          <th> @lang('bills.driver') </th>
          <th> @lang('bills.admin') </th>
          <th> @lang('bills.status') </th>
          <th> @lang('bills.type') </th>
          <th> @lang('bills.comment') </th>
          <th> @lang('bills.price') </th>
          <th> @lang('bills.created_at') </th>
        </tr>
      </thead>
      <tbody>
        @foreach ($bills as $bill)
        <tr>
          <td style="text-align: center">{{ $bill->number  }}</td>
          <td style="text-align: center">{{ optional($bill->driver)->name  }}</td>
          <td style="text-align: center">{{ optional($bill->admin)->name }}</td>

          <td style="text-align: center">
            @switch($bill->status)
            @case(0)
            <span class="badge bg-warning" > @lang('bills.waiting') </span>
            @break
            @case(1)
            <span class="badge bg-success" > @lang('bills.accepted') </span>
            @break
            @case(2)
            <span class="badge bg-danger" > @lang('bills.refused') </span>
            @break
            @endswitch
          </td>
          <td style="text-align: center">{{ optional($bill->type)['type_'.$lang]   }}</td>
          <td style="text-align: center">{{ $bill->comment }}</td>
          <td style="text-align: center"> {{ $bill->price }} </td>
          <td style="text-align: center">{{ $bill->created_at->toFormattedDateString() }}</td>
        </tr>
        @endforeach

      </tbody>
    </table>

    <hr>

  </main>

</body>
</html>