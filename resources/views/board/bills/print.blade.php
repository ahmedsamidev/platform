<?php 
 header("Content-type: text/css; charset: UTF-8");
$lang = session()->get('locale');
?>


<!DOCTYPE html>
<html lang="ar">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>{{ $bill->number }}</title>

  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

   <style>


    .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }

    a {
      color: #0087C3;
      text-decoration: none;
    }

    body {
      direction: rtl;
      position: relative;
      width: 100%;  
      height: 29.7cm; 
      margin: 0 auto; 
      color: #555555;
      background: #FFFFFF; 

      font-family: 'Almarai' , sans-serif;
      /*font-family: DejaVu Sans;*/


      /*font-family: 'Tajawal', sans-serif;*/
      font-size: 12px; 
      font-weight: bold;
    }

    header {
      padding: 10px 0;
      margin-bottom: 20px;
      border-bottom: 1px solid #AAAAAA;
    }


    #company {
      float: right;
      text-align: right;
    }


    #details {
      margin-bottom: 50px;
    }

    #client {
      padding-left: 6px;
      border-left: 6px solid #0087C3;
      float: left;
    }

    #client .to {
      color: #777777;
    }

    h2.name {
      font-size: 1.4em;
      font-weight: normal;
      margin: 0;
    }

    #invoice {
      float: right;
      text-align: right;
    }

    #invoice h1 {
      color: #0087C3;
      font-size: 2.4em;
      line-height: 1em;
      font-weight: normal;
      margin: 0  0 10px 0;
    }

    #invoice .date {
      font-size: 1.1em;
      color: #777777;
    }


    #thanks{
      font-size: 2em;
      margin-bottom: 50px;
    }

    #notices{
      padding-left: 6px;
      border-left: 6px solid #0087C3;  
    }

    #notices .notice {
      font-size: 1.2em;
    }

    footer {
      color: #777777;
      width: 100%;
      height: 30px;
      position: absolute;
      bottom: 0;
      border-top: 1px solid #AAAAAA;
      padding: 8px 0;
      text-align: center;
    }

    table {
      text-align: center;
    }


    table thead tr th {
      background-color: #3b8668;
      color: white;
    }


  </style>
</head>
<body dir="rtl">



	<img class="img-responsive img-thumbnail " width="300" height="300" src="{{ Storage::disk('s3')->url('bills/'.$bill->image) }}" alt="">


	<br>
	<br>
	
	<table class="table table-bordered table-hover" border='1'>
		<tbody dir="rtl" >			
			<tr>
				<th class="font-weight-bold text-dark">@lang('bills.number')</th>
				<td class="text-right"> {{ $bill->number }} </td>
			</tr>
			<tr>
				<th class="font-weight-bold text-dark">@lang('bills.admin')</th>
				<td class="text-right"> 
					<a href="{{ route('admins.show'  , ['admin' => $bill->admin_id ] ) }}"> {{ optional($bill->admin)->name }} 
						<span class="d-block font-weight-normal text-muted"> {{ optional(optional($bill->admin)->type)['name_'.$lang] }} </span>
					</a> 
				</td>
			</tr>
			<tr>
				<th class="font-weight-bold text-dark">@lang('bills.driver')</th>
				<td class="text-right">
					<a href="{{ route('drivers.show'  , ['driver' => $bill->driver_id ] ) }}"> {{ optional($bill->driver)->name }} 
					</a> 
				</td>
			</tr>
			<tr>
				<th class="font-weight-bold text-dark">@lang('bills.status')</th>
				<td class="text-right"> 
					@switch($bill->status)
					@case(0)
					<span class="badge bg-warning" > @lang('bills.waiting') </span>
					@break
					@case(1)
					<span class="badge bg-success" > @lang('bills.accepted') </span>
					@break
					@case(2)
					<span class="badge bg-danger" > @lang('bills.refused') </span>
					@break
					@endswitch
				</td>
			</tr>

			<tr>
				<th class="font-weight-bold text-dark"> @lang('bills.type') </th>
				<td class="text-right">	
					<span class="badge badge-info font-weight-bold" > {{ optional($bill->type)['type_'.$lang] }} </span> 

				</td>
			</tr>


			<tr>
				<th class="font-weight-bold text-dark"> @lang('bills.comment') </th>
				<td class="text-right">	
					{{ $bill->comment }}
				</td>
			</tr>

			<tr>
				<th class="font-weight-bold text-dark"> @lang('bills.price') </th>
				<td class="text-right">	
					{{ $bill->price }}
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold text-dark"> @lang('bills.created_at') </td>
				<td class="text-right"> {{ $bill->created_at->toFormattedDateString() }} - {{ $bill->created_at->diffForHumans() }} </td>
			</tr>

			<tr>
				<td class="font-weight-bold text-dark"> @lang('bills.updated_at') </td>
				<td class="text-right font-weight-bold">
					{{ $bill->updated_at->toFormattedDateString() }}
				</td>
			</tr>
		</tbody>
	</table>


</body>
</html>