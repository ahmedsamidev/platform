<?php
    header("Content-type: text/css; charset: UTF-8");
$lang = session()->get('locale');

?>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

  <style>
    th{
      color: white;
      background-color: #3b8668;
      text-align: center;
    }
  </style>
</head>
<body >
  <table>
   <thead>
     <tr>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >#</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >السائق</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >المدير</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >الحاله</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >نوع الفاتوره</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >التعليق</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >المبلغ</th>
       <th  style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >تاريخ الاضافه </th>
     </tr>
   </thead>
    <tbody>
      @foreach ($bills as $bill)
      <tr>
        <td style="text-align: center">{{$bill->number  }}</td>
        <td style="text-align: center">{{ optional($bill->driver)->name   }}</td>
        <td style="text-align: center">{{ optional($bill->admin)->name  }}</td>
        <td style="text-align: center">

          @switch($bill->status)
          @case(0)
          <span class="badge bg-warning" > @lang('bills.waiting') </span>
          @break
          @case(1)
          <span class="badge bg-success" > @lang('bills.accepted') </span>
          @break
          @case(2)
          <span class="badge bg-danger" > @lang('bills.refused') </span>
          @break
          @endswitch

        </td>
         <td style="text-align: center">{{ optional($bill->type)['type_'.$lang] }}</td>

        <td style="text-align: center"> {{$bill->comment }} </td>
        <td style="text-align: center">{{ $bill->price }}</td>
        <td style="text-align: center">{{ $bill->created_at->toFormattedDateString() }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
{{--   <table border="1" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >مجموع الكاش </th>
        <th style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >مجموع التوصيل</th>
        <th style="background-color:  #3b8668; color: #FFFFFF; text-align: center; font-weight: bold" >مجموع كى نت</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align: center" > {{ $bills->where('payment_method_id' , 1)->sum('order_price') }} </td>
        <td style="text-align: center" >{{ $bills->sum('delivery_price') }} </td>
        <td style="text-align: center" > {{  $bills->where('payment_method_id' , 2)->sum('order_price') }}</td>
      </tr>
    </tbody>
  </table> --}}

</body>

</html>


