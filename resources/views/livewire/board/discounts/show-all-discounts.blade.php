

<div class="row">
	<div class="col-md-12 mb-3">
		<div class="card">
{{-- 			<div class="card-header">
				@lang('discounts.search')
			</div> --}}
			<div class="card-body">
				<a  href="{{ route('discounts.create') }}" class="btn btn-primary float-right" >
					<i class="icon-plus3 "></i> @lang('discounts.add_new_discount')  </a>
				{{-- <button class="btn btn-dark float-right mr-2" data-toggle="collapse" data-target="#filters">
					<i class="icon-filter3"></i> @lang('discounts.advanced_search')
				</button> --}}
			{{-- 	<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<input type="text" id="search" wire:model="search" placeholder="@lang('discounts.search_discounts') ....." class="form-control" >
						</div>
					</div>
				</div> --}}
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card" >
			<div class="card-header header-elements-inline">
				<h5 class="card-title"> <i class="icon-scissors  mr-1"></i> @lang('discounts.discounts')</h5>
				<div class="header-elements">
					<div class="wmin-200">
						<select class="form-control form-control-select2 select" wire:model="paginate" >
							<option value="10"> 10 </option>
							<option value="15">15 </option>
							<option value="30">30</option>
							<option value="50">50</option>
							<option value="70">70</option>
							<option value="100">100</option>
							<option value="150">150</option>
						</select>
					</div>
				</div>
			</div>

			<table class="table datatable-responsive  text-center  table-bordered  table-hover ">
				<thead>
					<tr>
						<th> 

						</th>
						<th>#</th>
						<th> @lang('discounts.title') </th>
						<th> @lang('discounts.percentage') </th>
						<th> @lang('discounts.allowed_used_count') </th>
						<th> @lang('discounts.added_by') </th>
						<th> @lang('discounts.start_date') </th>
						<th> @lang('discounts.end_date') </th>
						<th> @lang('discounts.activation') </th>
						<th> @lang('discounts.created_at') </th>
					</tr>
				</thead>
				<tbody>
					@php
					$i =1 ;
					@endphp
					@foreach ($discounts as $discount)
					<tr>
						<td>
							<a href="#collapse-icon{{ $discount->id }}" class="text-default" data-toggle="collapse">
								<i class="icon-circle-down2"></i>
							</a>
						</td>
						<td  >{{ $i++ }}</td>
						<td> {{ $discount->title }} </td>
						<td> {{ $discount->discount_percentage }} %</td>
						<td> {{ $discount->allowed_used_count }} </td>
						<td> <a target="_blank" href="{{ route('admins.show'  , ['admin' => $discount->admin_id] ) }}"> {{ optional($discount->admin)->username }} </a> </td>
						<td> {{ $discount->starts_at->toDateTimeString() }} - <span class="text-muted">  {{ $discount->starts_at->diffForHumans() }} </span> </td>
						<td> {{ $discount->ends_at->toDateTimeString() }} - <span class="text-muted">  {{ $discount->ends_at->diffForHumans() }} </span> </td>
						<td>
							<input type="checkbox"  class="form-check-input-switchery" checked data-fouc>
							{{-- @switch($discount->active)
							@case(1)
							<span class="badge badge-primary"> @lang('discounts.active') </span>
							@break
							@case(0)
							<span class="badge badge-secondary"> @lang('discounts.inactive') </span>
							@break
							@endswitch --}}
						</td>
						<td>
							{{ $discount->created_at->toFormattedDateString() }} - <span class="text-muted"> {{ $discount->created_at->diffForHumans() }} </span>
						</td>
					</tr>
					<tr  class="collapse " id="collapse-icon{{ $discount->id }}" >
						<td colspan="100%">
							<div class="float-left">
								<a target="_blank"  data-popup="tooltip" title="@lang('discounts.discount_details')" href="{{ route('discounts.show',['discount' => $discount->id ] ) }}" class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">
									<i class="icon-eye2 text-primary-800"></i>
								</a>
								<a href="" data-id="{{ $discount->id }}"  data-popup="tooltip" title="@lang('discounts.delete_discount')" class=" delete_item btn btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 ml-2"><i class="icon-trash"></i>  </a>	
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div class="card-footer bg-light ">
				<div class="float-right" >
					{{ $discounts->links() }}
				</div>				
			</div>
		</div>



	</div>
</div>	




<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
{{-- <script src="{{ asset('board_assets/global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script> --}}

@push('scripts')
    @livewireScripts


<script>

	

	$(document).ready(function() {

		var status ;
		var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
		elems.forEach(function(html) {
			var switchery = new Switchery(html , { color: 'green'  , secondaryColor    : 'red' });
		});

		document.addEventListener("livewire:load", function (event) {
			livewire.hook('afterDomUpdate', () => {
				var status ;
				var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
				elems.forEach(function(html) {
					var switchery = new Switchery(html , { color: 'green'  , secondaryColor    : 'red' });
				});
			});
		});





		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		})

		Livewire.on('itemDeleted', itemId => {
			Toast.fire({
				icon: 'success',
				title: "@lang('discounts.deleted_success')", 
			});
		})

		$(document).on('click', 'a.delete_item' ,  function(event) {
			event.preventDefault();
			item_id = $(this).data('id');
			confirm_deletion(item_id);
		});

		function confirm_deletion(item_id) {
			Swal.fire({
				title: 'تاكيد الحذف ',
				text: "هل انت متاكد من حذف هذا الخصم ؟",
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'نعم',
				cancelButtonText: 'لا',
			}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emit('deleteItemConfirmed'  , item_id );
				}
			})
		}

	});




</script>

@endpush