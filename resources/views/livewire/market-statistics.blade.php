@php
$lang = session()->get('locale');
@endphp

<div>
		<div class="row">
		<div class="col-md-12">
			<div class="card "  >
				<div class="card-header bg-dark">
					@lang('trips.advanced_search')
				</div>
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label> @lang('markets.from') </label>
								<input type="date"  wire:model="from" class="form-control  ">
							</div>
							<div class="col-md-4">
								<label> @lang('markets.to') </label>
								<input type="date"  wire:model="to"  class="form-control  ">
							</div>
							<div class="col-md-4">
								<label> @lang('markets.branche') </label>
								<select  class="form-control" wire:model="branch">
									<option value="all"> @lang('branches.all_branches') </option>
									@foreach ($branches as $branch)
									<option value="{{ $branch->id }}"> {{ $branch['name'] }} </option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="col-sm-6 col-xl-3">
					<div class="card card-body">
						<div class="media">
							<div class="media-body">
								<h3 class="font-weight-semibold mb-0"> {{ $total_trip_count }} </h3>
								<span class="text-uppercase font-size-sm text-blue-600">  عدد الرحلات </span>
							</div>

							<div class="ml-3 align-self-center">
								<i class="icon-coin-dollar  icon-3x text-blue-400"></i>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-xl-3">
					<div class="card card-body">
						<div class="media">
							<div class="media-body">
								<h3 class="font-weight-semibold mb-0"> {{ $total_order_price }} </h3>
								<span class="text-uppercase font-size-sm text-blue-600">  اجمالى سعر الطلبات </span>
							</div>

							<div class="ml-3 align-self-center">
								<i class="icon-coin-dollar  icon-3x text-blue-400"></i>
							</div>
						</div>
					</div>
				</div>



				<div class="col-sm-6 col-xl-3">
					<div class="card card-body">
						<div class="media">
							<div class="media-body">
								<h3 class="font-weight-semibold mb-0"> {{ $total_cache_trip_count }} </h3>
								<span class="text-uppercase font-size-sm text-blue-600">  اجمالى رحلات الكاش  </span>
							</div>

							<div class="ml-3 align-self-center">
								<i class="icon-coin-dollar  icon-3x text-blue-400"></i>
							</div>
						</div>
					</div>
				</div>


				<div class="col-sm-6 col-xl-3">
					<div class="card card-body">
						<div class="media">
							<div class="media-body">
								<h3 class="font-weight-semibold mb-0"> {{ $total_knet_trip_count }} </h3>
								<span class="text-uppercase font-size-sm text-blue-600">  اجمالى رحلات الكى نت </span>
							</div>

							<div class="ml-3 align-self-center">
								<i class="icon-coin-dollar  icon-3x text-blue-400"></i>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-xl-3">
					<div class="card card-body">
						<div class="media">
							<div class="media-body">
								<h3 class="font-weight-semibold mb-0"> {{ $total_delivery_price }} </h3>
								<span class="text-uppercase font-size-sm text-blue-600">  اجمالى سعر التوصيل </span>
							</div>

							<div class="ml-3 align-self-center">
								<i class="icon-coin-dollar  icon-3x text-blue-400"></i>
							</div>
						</div>
					</div>
				</div>



			</div>
		</div>
	</div>

</div>
