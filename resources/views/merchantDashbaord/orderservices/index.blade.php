@php
    $lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('site.show_all_orders')
@endsection


@section('header')
<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.orders') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.orders') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')
    @include('merchantDashbaord.layout.partials._session')


    <div class="row tables-body table-inner order-page">
        <div class="col-md-12">
            <div class="page-header">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.orders') </h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    <div class="header-elements d-none py-0 mb-3 mb-md-0">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="filtter-order">
                <form action="">
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>من</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>الي</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>رقم الطب</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>العميل(الاسم/البريد الإلكترونى/الهاتف)</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <!-- /Col -->
                    </div>
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>حالة الطلب</label>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <select class="form-control">
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <select class="form-control">
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                            <option>غير محدد</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>حالة عملية الدفع</label>
                                <select class="form-control">
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                </select>
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>طريقة الطلب</label>
                                <select class="form-control select-sec">
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                    <option>غير محدد</option>
                                </select>
                            </div>
                        </div>
                        <!-- /Col -->
                    </div>
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label>الحد الادني والاعلي للكمية</label>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <input type="number" class="form-control" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <input type="number" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-6 col-sm-12"></div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-3 col-sm-12">
                            <div class="btns-group">
                                <button type="reset" class="btn btn-reset">إفراغ الحقول</button>
                                <button type="submit" class="btn">بحث</button>
                            </div>
                        </div>
                        <!-- /Col -->
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col-md-12 margin-small search-none">
            @include('merchantDashbaord.layout.messages')
            <div class="row">
                <div class="btn-top col-md-12 mb-3">
                    <button class="btn btn-primary float-left" >تصدير الي excel</button>
              </div>
            </div>
            <div class="card" >


                <table class="table tableNew datatable-responsive table-togglable table-bordered  text-center  table-hover ">
                    <thead class="bg-dark">
                    <tr>
                        <th> كود الطلب </th>
                        <th> العميل </th>
                        <th> طريقة الاستلام / <br />الوقت المقدر  </th>
                        <th> الفرع / <br /> المنطقة </th>
                        <th> طريقة الدفع </th>
                        <th> حالة التوصيل </th>
                        <th> المجموع </th>
                        <th> تاريخ الإضافة </th>
                        <th>الاجراءات</th>

                    </tr>
                    </thead>

                    <tbody>
                    @php
                        $i =1 ;
                    @endphp
                    @foreach ($orderservices as $orderservice)
                        <tr>

                            <td>1234568989</td>
                            <td><div class="name-table">اسم العميل <span>123456</span></div></td>
                            <td><div class="order-time">توصيل الطلب <span>10-10-2020</span> <span>12:00 AM</span></div></td>
                            <td>اسم الفرع</td>
                            <td>KNET <u class="info-span">ناجح</u></td>
                            <td><u class="info-span">تم التسليم</u></td>
                            <td>3.000 KWD</td>
                            <td><span>10-10-2020</span> <span>12:00 AM</span></td>
                            <td>
                                <div class="float-left flex-none">
                                    <a href="{{ route('orderservices.show'  , [$orderservice->id ] ) }}" class="btn">
                                        <i class="icon-info22"></i>
                                        <span>الفاتورة كاملة</span>
                                    </a>
                                    <a href="#" class="btn">
                                        <i class="icon-truck"></i>
                                        <span>حالة التوصيل</span>
                                    </a>
                                    <a href="#" class="btn">
                                        <i class="icon-printer"></i>
                                        <span>طباعة الفاتورة</span>
                                    </a>
                                </div>
                            </td>

                        </tr>

                        @endforeach

                        </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection


@section('styles')


@endsection

@section('scripts')

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/content_cards_header.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/touch.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/components_collapsible.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $(function() {
            $('a.delete_branch').on('click', function(event) {
                event.preventDefault();
                id = $(this).data('id');
                console.log(id);
                Swal.fire({

                    text: "هل انت متاكد من حذف الطلب",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'لا'
                }).then((result) => {
                    if (result.isConfirmed) {

                        name = "deleteFormNumber" + id;
                        $('form[name="'+ name +'"]').submit();

                    }
                })
            });
        });
    </script>
@endsection
