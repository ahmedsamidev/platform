@php
    $lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('merchantDashbaord.orders_show')
@endsection


@section('header')


<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.orders') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <a href="{{ route('orderservices.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('merchantDashbaord.orders') </a>
                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.show') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')


    <div class="col-md-12 tables-body table-inner">
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-right6 mr-2"></i>مشاهدة </h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                <div class="header-elements d-none py-0 mb-3 mb-md-0">
                    <div class="breadcrumb">
                        <a  href="#" class="btn btn-primary float-right" > <i class="icon-paragraph-justify2"></i> الرجوع الي قائمة المبيعات</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Account settings -->
        <div class="card">
            <div class="card-body">
                <div class="view-top row">
                    <!-- Col -->
                    <div class="col-md-6 col-sm-12">
                        <div class="details-img">
                            <div class="img">
                                <img src=" http://localhost/apphh/platform/public/uploads/merchantDashbaord/fVPR260GcHWn9pxzpR6oKwGew9mZPkY28Xqtz5rQ.jpg" />
                            </div>
                            <div class="details">
                                <h3>يوضع الاسم هنا</h3>
                                <ul>
                                    <li>الفرع: اسم الفرع</li>
                                    <li>الهاتف: 278912345678</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Col -->
                    <!-- Col -->
                    <div class="col-md-6 col-sm-12">
                        <div class="details-pill">
                            <div class="details">
                                <ul>
                                    <li><strong>الفاتورة رقم: </strong> 123456789</li>
                                    <li><strong>تاريخ الطلب: </strong> 01 jan 2021 10:30AM</li>
                                    <li><strong>موعد التسليم: </strong> 01 jan 2021 10:30AM</li>
                                    <li><strong>طريقة التسليم: </strong> تم التوصيل</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Col -->
                </div>

                <div class="view-content">
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-6 col-sm-12">
                            <h4>معلومات العميل</h4>
                            <table class="table  table-xs border-top-0 my-2">
                                <tbody>
                                    <tr>
                                        <th>الاسم</th>
                                        <td>اسم العميل</td>
                                    </tr>
                                    <tr>
                                        <th>رقم الهاتف</th>
                                        <td>1234567</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /Col -->
                        <!-- Col -->
                        <div class="col-md-6 col-sm-12">
                            <h4>تفاصيل الطلب</h4>
                            <table class="table  table-xs border-top-0 my-2">
                                <tbody>
                                    <tr>
                                        <th>حالة الدفع</th>
                                        <td>ناجح</td>
                                    </tr>
                                    <tr>
                                        <th>طريقة الدفع</th>
                                        <td>كي نت</td>
                                    </tr>
                                    <tr>
                                        <th>تاريخ الدفع</th>
                                        <td>01 jan 2021 10:30AM</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /Col -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4>تفاصيل الطلب</h4>
                            <table class="table table-view table-xs border-top-0 my-2">
                                <thead>
                                    <tr>
                                        <th>الرقم</th>
                                        <th>المنتج</th>
                                        <th>صورة المنتج</th>
                                        <th>الرمز الشريطي</th>
                                        <th>الخيار الفرعي (KWD)</th>
                                        <th>الكمية</th>
                                        <th>سعر الوحدة (KWD)</th>
                                        <th>المجموع (KWD)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>اسم المنتج</td>
                                        <td>
                                            <div class="img">
                                                <img src=" http://localhost/apphh/platform/public/uploads/merchantDashbaord/fVPR260GcHWn9pxzpR6oKwGew9mZPkY28Xqtz5rQ.jpg" />
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>الجبنة شيدر</td>
                                        <td>2</td>
                                        <td>0.950</td>
                                        <td>1.100</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>اسم المنتج</td>
                                        <td>
                                            <div class="img">
                                                <img src=" http://localhost/apphh/platform/public/uploads/merchantDashbaord/fVPR260GcHWn9pxzpR6oKwGew9mZPkY28Xqtz5rQ.jpg" />
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>الجبنة شيدر</td>
                                        <td>2</td>
                                        <td>0.950</td>
                                        <td>1.100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-12 col-sm-12">
                            <table class="table table-thr table-xs border-top-0 my-2">
                                <tbody>
                                    <tr>
                                        <th>المجموع</th>
                                        <td>2.500 KWD</td>
                                    </tr>
                                    <tr>
                                        <th>مصاريف التوصيل</th>
                                        <td>0.250 KWD</td>
                                    </tr>
                                    <tr>
                                        <th>خصم</th>
                                        <td>0.000 KWD</td>
                                    </tr>
                                    <tr class="total-table">
                                        <th>المجموع الكلي</th>
                                        <td>3.100 KWD</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /Col -->
                    </div>
                    
                    <div class="row">
                        <!-- Col -->
                        <div class="col-md-12 col-sm-12">
                            <h3>عنوان التوصيل</h3>
                            <table class="table table-thr margin-top-b table-xs border-top-0 my-2">
                                <tbody>
                                    <tr>
                                        <th>الدولة</th>
                                        <td>الكويت</td>
                                    </tr>
                                    <tr>
                                        <th>مدينة</th>
                                        <td>محافظة الحمراء</td>
                                    </tr>
                                    <tr>
                                        <th>المنطقة</th>
                                        <td>سعد عبدالله</td>
                                    </tr>
                                    <tr>
                                        <th>نوع مكان الاستلام</th>
                                        <td>house</td>
                                    </tr>
                                    <tr>
                                        <th>قطعة</th>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <th>شارع</th>
                                        <td>625</td>
                                    </tr>
                                    <tr>
                                        <th>منزل</th>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <th>العنوان</th>
                                        <td>ازرق 16</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /Col -->
                    </div>
                    

                    <div class="row">
                        <div class="col-md-12">
                            <h4>ماي فاتورة تفاصيل الدفع</h4>
                            <table class="table table-view table-xs border-top-0 my-2">
                                <thead>
                                    <tr>
                                        <th>معرف الفاتورة</th>
                                        <th>معرف المرجع</th>
                                        <th>حالة الفاتورة</th>
                                        <th>حالة الفاتورة</th>
                                        <th>مبلغ الدفع</th>
                                        <th>تاريخ الدفع</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>2345678</td>
                                        <td>2345678</td>
                                        <td>9886754</td>
                                        <td>Paid</td>
                                        <td>2.1 KWD</td>
                                        <td>01 jan 2021 10:30AM</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('styles')

@endsection

@section('scripts')
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places"></script>

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/typeahead_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/autocomplete_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/location.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/ui/prism.min.js') }}"></script> --}}

    {{-- <script src="{{ asset('board_assets/global_assets/js/demo_pages/picker_location.js') }}"></script> --}}


@endsection
