@php
    $lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('merchantDashbaord.add_new_product')
@endsection


@section('header')
<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.product') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <a href="{{ route('products.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('merchantDashbaord.product') </a>
                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.add_new_product') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.product') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a  href="#" class="btn btn-primary float-right" > <i class="icon-paragraph-justify3  mr-1"></i> عودة الي قائمة المنتجات</a>
                </div>
            </div>
        </div>
    </div>
    <div class="tables-body table-inner great-page">
        <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#creat">تفاصيل المنتج</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#creat2">المعلومات التجارية</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#creat3">إدارة الخيارات الإضافية</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#creat4">الافرع المتاحة</a></li>
        </ul>
        <!-- <form action="{{ route('products.store') }}" method="POST"  enctype="multipart/form-data" > -->
            <div class="tab-content">
                <!-- Tab1 -->
                <div id="creat" class="tab-pane active">
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>اسم المنتج</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>

                            
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>اسم المنتج (العربية)</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>التصنيف</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <select class="form-control">
                                        <option>التصنيف</option>
                                        <option>التصنيف</option>
                                        <option>التصنيف</option>
                                        <option>التصنيف</option>
                                    </select>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>امر الترتيب</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="number" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>وحدة البيع</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>الكلمات المفتاحية</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>الصورة</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <div class="row">
                                        <!-- Col -->
                                        <div class="col-md-8 col-sm-12">
                                            <input class="form-control image" name="image" type="file" id="image">
                                        </div>
                                        <!-- /Col -->
                                        <!-- Col -->
                                        <div class="col-md-4 col-sm-12">
                                            <img src="http://localhost/apphh/platform/public/uploads/merchantDashbaord/no_image.png" style="width: 100px"
                                            class="img-thumbnail image-preview" alt="">
                                        </div>
                                        <!-- /Col -->
                                    </div>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>الوصف</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <textarea class="form-control"></textarea>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>الوصف (العربية)</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <textarea class="form-control"></textarea>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input class="btn btn-primary" type="submit" value="حفظ" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Tab2 -->
                <div id="creat2" class="tab-pane fade">
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>سعر البيع</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12 flex-input">
                                    <input type="number" class="form-control" />
                                    <span>KWD</span>
                                </div>
                                <!-- /Col -->
                            </div>

                            
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>تكلفة الوحدة (إن وجدت)</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12 flex-input">
                                    <input type="number" class="form-control" />
                                    <span>KWD</span>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>تكلفة الشحن (إن وجدت)</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12 flex-input">
                                    <input type="number" class="form-control" />
                                    <span>KWD</span>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>حد الشراء</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>وحدة البيع</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>الرمز الشريطي</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>خصم (%)</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <input type="text" class="form-control" />
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input class="btn btn-primary" type="submit" value="حفظ" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Tab3 -->
                <div id="creat3" class="tab-pane fade">
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <label>اذا كنت بحاجة الي إضافة خيارات اخري من هذا المنتج لعملائك اضغط هنا</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <a href="#" class="btn">
                                        <i class="icon-plus3"></i>
                                        <span>إضافة خيارات إدخال العملاء</span>
                                    </a>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input class="btn btn-primary" type="submit" value="حفظ" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <!-- Tab4 -->
                <div id="creat4" class="tab-pane fade">
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group row">
                                <!-- Col -->
                                <div class="col-md-4 col-sm-12">
                                    <label>متوفر فى جميع الافرع</label>
                                </div>
                                <!-- /Col -->
                                <!-- Col -->
                                <div class="col-md-8 col-sm-12">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch3" checked >
                                        <label class="onoffswitch-label" for="myonoffswitch3">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                                <!-- /Col -->
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input class="btn btn-primary" type="submit" value="حفظ" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <!-- </form>  -->
</div>
@endsection


@section('styles')

@endsection

@section('scripts')
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places"></script>

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/typeahead_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/autocomplete_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/location.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/ui/prism.min.js') }}"></script> --}}

    {{-- <script src="{{ asset('board_assets/global_assets/js/demo_pages/picker_location.js') }}"></script> --}}

    <script>
        $(function() {
            // $("#firstname").attr("disabled", "disabled");


            // image preview
            $(".image").change(function () {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.image-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }

            });
            $('.select').select2({
                minimumResultsForSearch: Infinity
            });

            $('.form-input-styled').uniform({
                fileButtonClass: 'action btn bg-primary'
            });


            var _componentSwitchery = function() {
                if (typeof Switchery == 'undefined') {
                    console.warn('Warning - switchery.min.js is not loaded.');
                    return;
                }


                var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html);
                });

                var primary = document.querySelector('.form-check-input-switchery-primary');
                var switchery = new Switchery(primary, { color: '#2196F3' });
            };
            // Bootstrap switch
            var _componentBootstrapSwitch = function() {
                if (!$().bootstrapSwitch) {
                    console.warn('Warning - switch.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.form-check-input-switch').bootstrapSwitch();
            };
            _componentSwitchery();



        });
    </script>
@endsection
