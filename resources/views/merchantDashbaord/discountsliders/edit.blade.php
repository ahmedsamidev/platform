@php
$lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
@lang('merchantDashbaord.edit_disslider')
@endsection


@section('header')
<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.edit_disslider') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <a href="{{ route('dissliders.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('merchantDashbaord.edit_disslider') </a>
                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.edit_disslider') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')

<div class="row tables-body table-inner great-page">
    <div class="col-md-12">
        <!-- Account settings -->

        @include('merchantDashbaord.layout.messages')
        <div class="card">
            <div class="card-header bg-dark header-elements-inline">
                <h5 class="card-title"> @lang('merchantDashbaord.edit_disslider') </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            {!! Form::model($disslider, ['route' => ['dissliders.update', $disslider->id], 'method' => 'patch', 'files' => true]) !!}
            @method('patch')
            <div class="card-body">
                @csrf
                <fieldset>
<!--                    <legend class="font-weight-bold"> <span class="text-primary"> @lang('merchantDashbaord.edit_disslider') </span> </legend>-->
                    <div class="row">
                        <!-- Name Ar Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('name_ar', __('merchantDashbaord.name_ar')) !!}
                            {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                            @error('name_ar')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>

                        <!-- Name En Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('name_en', __('merchantDashbaord.name_en')) !!}
                            {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
                            @error('name_en')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>

                        <!-- Discount Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('image', __('merchantDashbaord.image')) !!}
                            <input type="file" name="image" id="input-file-max-fs" class="form-control dropify image" data-default-file="{{asset('uploads/admins/'.$disslider->image)}}" data-max-file-size="2M" />

                            @error('image')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-4">
                            <img src="{{asset('uploads/admins/'.$disslider->image)}}" style="width: 100px;" class="img-thumbnail image-preview" alt="">
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('expire', __('merchantDashbaord.expire')) !!}
                            {!! Form::date('expire',null,['class' => 'form-control']) !!}
                            @error('expire')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('barcode', __('merchantDashbaord.barcode')) !!}
                            {!! Form::text('barcode',null,['class' => 'form-control']) !!}
                            @error('barcode')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('count_use', __('merchantDashbaord.count_use')) !!}
                            {!! Form::number('count_use',null,['class' => 'form-control']) !!}
                            @error('count_use')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('repeatation', __('merchantDashbaord.repeatation')) !!}
                            <select class="form-control" name="repeatation">
                                @if($disslider->repeatation == 'جميع العملاء' )
                                <option value="{{$disslider->id}}" selected> @lang('merchantDashbaord.all_clients') </option>
                                <option> @lang('merchantDashbaord.all_visitors') </option>
                                <option> @lang('merchantDashbaord.every_client') </option>
                                @endif
                                @if($disslider->repeatation == 'جميع الزائرين')
                                <option value="{{$disslider->id}}" selected> @lang('merchantDashbaord.all_visitors') </option>
                                <option> @lang('merchantDashbaord.all_clients') </option>
                                <option> @lang('merchantDashbaord.every_client') </option>
                                @endif
                                @if($disslider->repeatation == 'لكل عميل')
                                <option value="{{$disslider->id}}" selected> @lang('merchantDashbaord.every_client') </option>
                                <option> @lang('merchantDashbaord.all_clients') </option>
                                <option> @lang('merchantDashbaord.all_visitors') </option>
                                @endif
                            </select>
                            @error('repeatation')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            @lang('merchantDashbaord.free_shipping')
                            <div class="onoffswitch">
                                
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch3" checked {{$disslider->free_shipping=='1' ? 'checked' : ''}}>
                                <label class="onoffswitch-label" for="myonoffswitch3">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('value_discount', __('merchantDashbaord.value_discount')) !!}
                            {!! Form::number('value_discount',null,['class' => 'form-control']) !!}
                            @error('value_discount')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('discount_type', __('merchantDashbaord.discount_type')) !!}
                            <select class="form-control" name="discount_type">
                                @if($disslider->discount_type == 'نسبة مئوية')
                                <option value="{{$disslider->id}}"> {{$disslider->discount_type}} </option>
                                <option> @lang('merchantDashbaord.quantity') </option>
                                @endif
                                @if($disslider->discount_type == 'كمية')
                                <option value="{{$disslider->id}}"> {{$disslider->discount_type}} </option>
                                <option> @lang('merchantDashbaord.percent') </option>
                                @endif
                            </select>
                            @error('discount_type')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('min_cost', __('merchantDashbaord.min_cost')) !!}
                            {!! Form::number('min_cost',null,['class' => 'form-control']) !!}
                            @error('min_cost')
                            <label class="text-danger font-weight-bold "> {{ $message }} </label>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            @lang('merchantDashbaord.status_slider')
                            <div class="onoffswitch">
                                
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch2" checked {{$disslider->status_slider=='1' ? 'checked' : ''}} >
                                <label class="onoffswitch-label" for="myonoffswitch2">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group col-sm-4">
                            @lang('merchantDashbaord.current_status')
                            <div class="onoffswitch">
                                
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked {{$disslider->current_status=='1' ? 'checked' : ''}} >
                                <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>

                    </div>

                </fieldset>
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('exproducts.index') }}" class="btn btn-default">Cancel</a>
            </div>


        </div>
        <!-- /account settings -->
    </div>
</div>





@endsection


@section('styles')

@endsection

@section('scripts')
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places"></script>

<script src="{{ asset('board_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
{{-- <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/typeahead_addresspicker.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/autocomplete_addresspicker.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/location.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/ui/prism.min.js') }}"></script> --}}

{{-- <script src="{{ asset('board_assets/global_assets/js/demo_pages/picker_location.js') }}"></script> --}}

<script>
    $(function() {
        // $("#firstname").attr("disabled", "disabled");


        // image preview
        $(".image").change(function() {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
        $('.select').select2({
            minimumResultsForSearch: Infinity
        });

        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-primary'
        });


        var _componentSwitchery = function() {
            if (typeof Switchery == 'undefined') {
                console.warn('Warning - switchery.min.js is not loaded.');
                return;
            }


            var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html);
            });

            var primary = document.querySelector('.form-check-input-switchery-primary');
            var switchery = new Switchery(primary, {
                color: '#2196F3'
            });
        };
        // Bootstrap switch
        var _componentBootstrapSwitch = function() {
            if (!$().bootstrapSwitch) {
                console.warn('Warning - switch.min.js is not loaded.');
                return;
            }

            // Initialize
            $('.form-check-input-switch').bootstrapSwitch();
        };
        _componentSwitchery();



    });
</script>
@endsection