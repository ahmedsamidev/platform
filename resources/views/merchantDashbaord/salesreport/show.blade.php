@php
$lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
@lang('merchantDashbaord.sales_report')
@endsection


@section('header')
<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.sales_report') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.sales_report') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')
@include('merchantDashbaord.layout.partials._session')


<div class="row tables-body table-inner">
    <div class="col-md-12">
        @include('merchants.layout.messages')
        <div class="row">
            <div class="col-md-12 mb-3 title-0">
                <h3 class="title">
                    @lang('merchantDashbaord.products_more_sales')
                </h3>
            </div>
        </div>
        <div class="card">

            <table class="table datatable-responsive table-togglable table-bordered text-center   table-hover ">
                <thead class="bg-dark">
                    <tr>

                        <th> @lang('merchantDashbaord.name_ar') </th>
                        <th> @lang('merchantDashbaord.price') </th>
                        <th> @lang('merchantDashbaord.order') </th>

                    </tr>
                </thead>

                <tbody>
                    @foreach ($products as $product)
                    <tr>
                        <td>{{$product->name_ar}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->order}}</td>
                    </tr>

                    @endforeach

                </tbody>
            </table>

        </div>


        <div class="row">
            <div class="col-md-12 mb-3">
                @lang('merchantDashbaord.customers_store')
            </div>
        </div>

        <table class="table datatable-responsive table-togglable table-bordered text-center   table-hover ">
            <thead class="bg-dark">
                <tr>

                    <th> @lang('merchantDashbaord.name') </th>
                    <th> @lang('merchantDashbaord.email') </th>
                    <th> @lang('merchantDashbaord.phone') </th>
                    <th> @lang('merchantDashbaord.city') </th>


                </tr>
            </thead>

            @foreach ($clients as $client)
            <tbody class="bg-white">
                <tr>
                    <td>{{$client->name}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->phone}}</td>
                    <td>{{$client->city->name_ar}}</td>
                </tr>
                @endforeach

            </tbody>
        </table>



    </div>
</div>

@endsection


@section('styles')


@endsection

@section('scripts')

<script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/demo_pages/content_cards_header.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/touch.min.js') }}"></script>
<script src="{{ asset('board_assets/global_assets/js/demo_pages/components_collapsible.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(function() {
        $('a.delete_branch').on('click', function(event) {
            event.preventDefault();
            id = $(this).data('id');
            console.log(id);
            Swal.fire({

                text: "هل انت متأكد من الحذف",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'لا'
            }).then((result) => {
                if (result.isConfirmed) {

                    name = "deleteFormNumber" + id;
                    $('form[name="' + name + '"]').submit();

                }
            })
        });
    });
</script>
@endsection