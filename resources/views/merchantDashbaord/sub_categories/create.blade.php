@php
    $lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('merchantDashbaord.add_new_category')
@endsection


@section('header')

<!--

    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.categories') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <a href="{{ route('sub_categories.index') }}" class="breadcrumb-item"><i class="icon-users4 mr-2"></i>  @lang('merchantDashbaord.categories') </a>
                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.add_category') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- Account settings -->
            <div class="page-header">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.add_new_category') </h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    <div class="header-elements d-none py-0 mb-3 mb-md-0">
                        <div class="breadcrumb">
                            <a  href="#" class="btn btn-primary float-right" > <i class="icon-file-text2  mr-1"></i> إدارة التصنيفات </a>
                        </div>
                    </div>
                </div>
            </div>

            @include('merchantDashbaord.layout.messages')
            <div class="card form-creat">
                <!-- <div class="card-header bg-dark header-elements-inline">
                    <h5 class="card-title"> </h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div> -->
                <form action="{{ route('subCategories.store') }}" method="POST"  enctype="multipart/form-data" >
                    <div class="card-body">
                        @csrf
                        <fieldset>
<!--                            <legend class="font-weight-bold"> <span class="text-primary"> @lang('merchantDashbaord.category') </span> </legend>-->
                                <!-- <div class="row"> -->
                                    <!-- Name Ar Field -->
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            {!! Form::label('name_ar',__('merchantDashbaord.name_ar')) !!}
                                        </div>
                                        <div class="col-sm-8">
                                            {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                                            @error('name_ar')
                                            <label class="text-danger font-weight-bold " > {{ $message }} </label>
                                            @enderror
                                        </div>
                                    </div>

                                    <!-- Name En Field -->
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            {!! Form::label('name_en', __('merchantDashbaord.name_en'))  !!}
                                        </div>
                                        <div class="col-sm-8">
                                            {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
                                            @error('name_en')
                                            <label class="text-danger font-weight-bold " > {{ $message }} </label>
                                            @enderror
                                        </div>
                                    </div>





                                    <!-- Discount Field -->
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            {!! Form::label('discount', __('merchantDashbaord.discount'))!!}
                                        </div>
                                        <div class="col-sm-8">
                                            {!! Form::number('discount', null, ['class' => 'form-control']) !!}
                                            @error('discount')
                                            <label class="text-danger font-weight-bold " > {{ $message }} </label>
                                            @enderror
                                        </div>
                                    </div>

                                    <!-- Order Field -->
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            {!! Form::label('order',__('merchantDashbaord.order')) !!}
                                        </div>
                                        <div class="col-sm-8">
                                            {!! Form::number('order', null, ['class' => 'form-control']) !!}
                                            @error('order')
                                            <label class="text-danger font-weight-bold " > {{ $message }} </label>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                           <label>الحالة</label>
                                        </div>
                                        <div class="col-sm-8">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch3" checked />
                                            <label class="onoffswitch-label" for="myonoffswitch3">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            {!! Form::label('image', __('merchantDashbaord.image')) !!}
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    {!! Form::file('image',['class' => 'form-control image']) !!}
                                                    @error('image')
                                                    <label class="text-danger font-weight-bold " > {{ $message }} </label>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-4">
                                                    <img src="{{asset('uploads/merchantDashbaord/no_image.png')}}" style="width: 100px"
                                                    class="img-thumbnail image-preview" alt="">
                                                </div>
                                        </div>
                                    </div>

                                    <input type="hidden" value="1" name="status">


                                    <!-- Submit Field -->


                                <!-- </div> -->


                        </fieldset>
                    </div>
                    <div class="form-group btns-form col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{{ route('subCategories.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                </form>

            </div>
            <!-- /account settings -->
        </div>
    </div>

@endsection


@section('styles')

@endsection

@section('scripts')
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places"></script>

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/typeahead_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/autocomplete_addresspicker.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/pickers/location/location.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/ui/prism.min.js') }}"></script> --}}

    {{-- <script src="{{ asset('board_assets/global_assets/js/demo_pages/picker_location.js') }}"></script> --}}

    <script>
        $(function() {
            // $("#firstname").attr("disabled", "disabled");


            // image preview
            $(".image").change(function () {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.image-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }

            });
            $('.select').select2({
                minimumResultsForSearch: Infinity
            });

            $('.form-input-styled').uniform({
                fileButtonClass: 'action btn bg-primary'
            });


            var _componentSwitchery = function() {
                if (typeof Switchery == 'undefined') {
                    console.warn('Warning - switchery.min.js is not loaded.');
                    return;
                }


                var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html);
                });

                var primary = document.querySelector('.form-check-input-switchery-primary');
                var switchery = new Switchery(primary, { color: '#2196F3' });
            };
            // Bootstrap switch
            var _componentBootstrapSwitch = function() {
                if (!$().bootstrapSwitch) {
                    console.warn('Warning - switch.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.form-check-input-switch').bootstrapSwitch();
            };
            _componentSwitchery();



        });
    </script>
@endsection
