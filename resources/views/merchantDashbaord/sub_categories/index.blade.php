@php
    $lang = session()->get('locale');
@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('branches.show_all_branches')
@endsection


@section('header')



@endsection
@section('content')

    <div class="row tables-body table-inner order-page">
        <div class="col-md-12">
            <div class="page-header">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.categories') </h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    <div class="header-elements d-none py-0 mb-3 mb-md-0">
                        <div class="breadcrumb">
                            <a  href="{{ route('subCategories.create') }}" class="btn btn-primary float-right" > <i class="icon-plus3  mr-1"></i>@lang('merchantDashbaord.add_new_category') </a>
                        </div>
                    </div>
                </div>
            </div>

            @include('merchants.layout.messages')
            <div class="card" >


                <table class="table tableNew datatable-responsive table-togglable table-bordered text-center   table-hover ">
                    <thead class="bg-dark">
                        <tr>
                            <th> @lang('merchantDashbaord.name_ar') </th>
                            <th> @lang('merchantDashbaord.name_en') </th>

                            <th> @lang('merchantDashbaord.order') </th>
                            <th> @lang('merchantDashbaord.status') </th>
                            <th> @lang('merchantDashbaord.created_at') </th>
                            <th>الاجراءات</th>
                        </tr>
                    </thead>
                    @php
                        $i =1 ;
                    @endphp
                    @foreach ($subCategories as $subCategory)
                        <tr>
                            <td>{{$subCategory->name_ar}}</td>
                            <td>{{$subCategory->name_en}}</td>
                            <td>{{@$subCategory->order}}</td>



                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch3" {{ $subCategory->
                                    status == 1 ? 'checked' : '' }} >
                                    <label class="onoffswitch-label" for="myonoffswitch3">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </td>

                            <td >{{ $subCategory->created_at->toFormattedDateString() }} - {{ $subCategory->created_at->diffForHumans() }} </td>
                            <td>
                                <div class="float-left">
                                    <a  href="{{ route('subCategories.edit' , [ $subCategory->id ] ) }}" class="btn alpha-warning">
                                        <i class="icon-pencil7 text-warning-800"></i>
                                        <span>تعديل</span>
                                    </a>
                                    <a href="" data-id="{{ $subCategory->id }}" class=" delete_branch btn">
                                        <i class="icon-trash"></i>  
                                        <span>حذف</span>
                                    </a>

                                    <form name="deleteFormNumber{{ $subCategory->id }}" action="{{ route('subCategories.destroy' , [$subCategory->id ]) }}" method="POST" >

                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </div>
                            </td>

                        </tr>




                    @endforeach

                    </tbody>
                </table>
                <div class="btn-bottom">
                    <button class="csv-btn"><i class="icon-file-text"></i><span>csv</span></button>
                    <button class="xls-btn"><i class="icon-file-excel"></i><span>xls</span></button>
                    <button class="update-btn"><i class="icon-sync"></i><span>تحديث</span></button>
                </div>
            </div>

        </div>
    </div>

@endsection


@section('styles')


@endsection

@section('scripts')

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/content_cards_header.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/touch.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/components_collapsible.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $(function() {


            $('a.delete_branch').on('click', function(event) {
                event.preventDefault();
                id = $(this).data('id');
                console.log(id);
                Swal.fire({

                    text: "هل انت متاكد من رغبتك فى حذف الفرع",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'لا'
                }).then((result) => {
                    if (result.isConfirmed) {

                        name = "deleteFormNumber" + id;
                        $('form[name="'+ name +'"]').submit();

                    }
                })
            });
        });
    </script>
@endsection
