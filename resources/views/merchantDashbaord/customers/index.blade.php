@php
    $lang = session()->get('locale');

@endphp
@extends('merchantDashbaord.layout.master')
@section('title')
    @lang('merchantDashbaord.customers_store')
@endsection


@section('header')
<!--
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.customers_store') </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none py-0 mb-3 mb-md-0">
                <div class="breadcrumb">
                    <a href="{{ route('merchants.board') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>  @lang('board.board') </a>

                    <span class="breadcrumb-item active"> @lang('merchantDashbaord.customers_store') </span>
                </div>
            </div>
        </div>
    </div>
-->
@endsection

@section('content')
    @include('merchantDashbaord.layout.partials._session')


    <div class="row tables-body table-inner">
        <div class="col-md-12">
            <div class="page-header">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-right6 mr-2"></i> @lang('merchantDashbaord.customers_store') </h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    <div class="header-elements d-none py-0 mb-3 mb-md-0">
                        <div class="breadcrumb">
                            <a  href="#" class="btn btn-primary float-right" > <i class="icon-plus3  mr-1"></i> إضافة عميل</a>
                        </div>
                    </div>
                </div>
            </div>
            @include('merchants.layout.messages')
            <div class="row">
                <div class="col-md-12 mb-3">
                    <a  href="{{ route('clients.create') }}" class="btn btn-primary float-right" > <i class="icon-plus3  mr-1"></i>@lang('merchantDashbaord.add_customer') </a>
                </div>
            </div>
                  <div class="card" >

                <table class="table tableNew datatable-responsive table-togglable table-bordered text-center   table-hover ">
                    <thead class="bg-dark">
                    <tr>
<!--                        <th>#</th>-->
                        <th> @lang('merchantDashbaord.name') </th>
                        <th> @lang('merchantDashbaord.email') </th>
                        <th> @lang('merchantDashbaord.phone') </th>
                        <th> @lang('merchantDashbaord.city') </th>
                        <th> @lang('merchantDashbaord.created_at') </th>
<!--                        <th> @lang('merchantDashbaord.show') </th>-->
                        <th>الاجراءات</th>

                    </tr>
                    </thead>
                    @php
                        $i =1 ;
                    @endphp

                    @foreach ($clients as $client)
                      <tbody class="bg-white">
                        <tr>
<!--
                          <td >
                              <a href="#collapse-icon{{ $client->id }}" class="text-default" data-toggle="collapse">
                                  <i class="icon-circle-down2"></i>
                              </a>

                          </td>
-->


                            <td> {{$client->name}} </td>
                            <td> {{$client->email}} </td>
                            <td> {{$client->phone}} </td>
                            <td> {{$client->city->name_ar}} </td>

                            <td>{{ $client->created_at->toFormattedDateString() }} - {{ $client->created_at->diffForHumans() }} </td>
                            <td>
                                <div class="float-left flex-d">
                                    <a  href="{{ route('clients.show'  , [$client->id ] ) }}" class="btn btn-outline">
                                        <i class="icon-paperplane text-primary-800"></i>
                                        <span>مشاهدة</span>
                                    </a> 
                                    <a  href="{{ route('clients.edit' , [ $client->id ] ) }}" class="btn alpha-warning">
                                        <i class="icon-pencil7 text-warning-800"></i>
                                        <span>تعديل</span>
                                    </a>
                                    <a href="" data-id="{{ $client->id }}" class=" delete_branch btn">
                                        <i class="icon-trash"></i>  
                                        <span>حذف</span>
                                    </a>

                                    <form name="deleteFormNumber{{ $client->id }}" action="{{ route('clients.destroy' , [$client->id ]) }}" method="POST" >

                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </div>
                            </td>

                        </tr>
<!--

                        <tr class="collapse " id="collapse-icon{{ $client->id }}" >
                            <td colspan="100%" >
                                <div class="float-left">
                                    <a  href="{{ route('clients.show'  , [$client->id ] ) }}" class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">
                                        <i class="icon-eye2 text-primary-800"></i>
                                    </a>
                                    <a  href="{{ route('clients.edit' , [ $client->id ] ) }}" class="btn alpha-warning border-warning text-warning-800 btn-icon ml-2">
                                        <i class="icon-pencil7 text-warning-800"></i>
                                    </a>
                                    <a href="" data-id="{{ $client->id }}" class=" delete_branch btn btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 ml-2"><i class="icon-trash"></i>  </a>

                                    <form name="deleteFormNumber{{ $client->id }}" action="{{ route('clients.destroy' , [$client->id ]) }}" method="POST" >

                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </div>
                            </td>
                        </tr>

-->

                        @endforeach

                        </tbody>

                </table>
                <div class="btn-bottom">
                    <button class="csv-btn"><i class="icon-file-text"></i><span>csv</span></button>
                    <button class="xls-btn"><i class="icon-file-excel"></i><span>xls</span></button>
                    <button class="update-btn"><i class="icon-sync"></i><span>تحديث</span></button>
                </div>
            </div>

        </div>
    </div>

@endsection


@section('styles')


@endsection

@section('scripts')

    <script src="{{ asset('board_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/content_cards_header.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/plugins/extensions/jquery_ui/touch.min.js') }}"></script>
    <script src="{{ asset('board_assets/global_assets/js/demo_pages/components_collapsible.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $(function() {
            $('a.delete_branch').on('click', function(event) {
                event.preventDefault();
                id = $(this).data('id');
                console.log(id);
                Swal.fire({

                    text: "هل انت متاكد من رغبتك فى حذف الفرع",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'لا'
                }).then((result) => {
                    if (result.isConfirmed) {

                        name = "deleteFormNumber" + id;
                        $('form[name="'+ name +'"]').submit();

                    }
                })
            });
        });
    </script>
@endsection
