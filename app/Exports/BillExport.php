<?php

namespace App\Exports;

use App\Models\Bill;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class BillExport implements FromView  , WithHeadings , ShouldAutoSize 
{
    protected $bills;


	public function __construct($bills)
	{
		$this->bills = $bills;
	}


    public function view(): View
    {
        return view('board.bills.report-excel-view', [
            'bills' => $this->bills
        ]);
    }

    public function headings(): array
    {
    	return [
    		'#',
    		'السائق'  , 
    		'المدير' ,
            'الحاله'  , 
            'نوع الفاتوره'  , 
    		'التعليق'  , 
    		'المبلغ',
    		'تاريخ الاضافه ',
    	];
    }
}
