<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverIncome extends Model
{
    


    public function driver()
    {
    	return $this->belongsTo(Driver::class);
    }


    public function trip()
    {
    	return $this->belongsTo(Trip::class);
    }


}
