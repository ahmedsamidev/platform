<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountGovernorate extends Model
{
    

    protected $fillable = ['discount_id' , 'governorate_id'];


    public function governorate()
    {
    	return $this->belongsTo(Governorate::class);
    }
}
