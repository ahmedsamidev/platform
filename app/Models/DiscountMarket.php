<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountMarket extends Model
{
    
    protected $fillable = ['market_id' , 'discount_id'];


    public function market()
    {
    	return $this->belongsTo(Market::class);
    }

}
