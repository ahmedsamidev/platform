<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $dates = [
		'starts_at',
		'ends_at'
	];



	public function admin()
	{
		return $this->belongsTo(Admin::class);
	}

	public function governorates()
	{
		return $this->hasMany(DiscountGovernorate::class );
		
	}


	public function markets()
	{
		return $this->hasMany(DiscountMarket::class );
	}
}
