<?php

namespace App\Http\Requests\Board\Discounts;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percentage' => 'required' , 
            'start_date' => 'required' , 
            'start_time' => 'required' , 
            'end_date' => 'required' , 
            'end_time' => 'required' , 
            'title' => 'nullable' , 
            'discount_for' => 'required' , 
            'allowed_used_count' => 'nullable' , 
            'specific_market' => 'nullable' , 
            'specific_area'  => 'nullable', 
            'markets' => '' , 
            'customers' => '' , 
            'governorates' => ''
        ];
    }
}
