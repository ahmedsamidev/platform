<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Trip;
use App\Models\Branch;
use Livewire\WithPagination;
class MarketStatistics extends Component
{


	use WithPagination;

	public $paginate = 10;
	public $from = 'all';
	public $to = 'all';
	public $branch = 'all';
	public $total_order_price = 0;
	public $total_delivery_price = 0;
	public $total_trip_count = 0;
	public $total_cache_trip_count = 0;
	public $total_knet_trip_count = 0;


	public function updatedPaginate()
	{
		$this->resetPage();
	}



	public function updatedDate()
	{
		$this->resetPage();
	}

	public function updatedFrom()
	{
		$this->resetPage();
	}


	public function updatedTo()
	{
		$this->resetPage();
	}

	public function updatedBranch()
	{
		$this->resetPage();
	}




	protected $paginationTheme = 'bootstrap';
	public function render()
	{
		$market_id = 10;


		$trips = Trip::query()->where('sender_type' , 'market')->where('market_id' , $market_id);
		if ($this->from != 'all') {
			$trips = $trips->whereDate('created_at' , '>='  , $this->from );
		}
		if ($this->to != 'all') {
			$trips = $trips->whereDate('created_at' , '<='  , $this->to );
		}

		if ($this->branch != 'all') {
			$trips = $trips->where('branch_id' ,  $this->branch );
		}

		$trips = $trips->get();


	

		$this->total_order_price  =  $trips->sum('order_price') ;
		$this->total_delivery_price  = $trips->sum('delivery_price')  ;
		$this->total_cache_trip_count  = $trips->where('payment_method_id' , 1 )->sum('order_price')  ;
		$this->total_knet_trip_count  =  $trips->where('payment_method_id' , 2 )->sum('order_price');
		$this->total_trip_count  = $trips->count()  ;


		$branches = Branch::where('market_id' , $market_id)->get();
		return view('livewire.market-statistics' , compact('branches') );
	}



}
