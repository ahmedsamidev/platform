<?php

namespace App\Http\Livewire\Board;

use Livewire\Component;
use App\Models\Bill;
use App\Models\Trip;
use App\Models\Driver;
use Livewire\WithPagination;
use App\Exports\BillExport;
use Excel;
use PDF;
class Bills extends Component
{
	use WithPagination;

	public $paginate = 10;
	public $from = 'all';
	public $to = 'all';
	public $driver_id = 'all';
	public $status = 'all';
	public $total_accepted_bills_count= 0;
	public $total_refused_bills_count= 0;
	public $total_pending_bills_count= 0;
	public $total_accepted_bills_total_money_amount= 0;
	public $total_refused_bills_total_money_amount= 0;
	public $total_pending_bills_total_money_amount= 0;



	public function updatedPaginate()
	{
		$this->resetPage();
	}



	public function updatedDate()
	{
		$this->resetPage();
	}


	public function pdf()
	{
		$bills = Bill::query()->with(['driver'  , 'type' , 'admin' ]);
		if ($this->from != 'all') {
			$bills = $bills->whereDate('created_at' , '>='  , $this->from );
		}
		if ($this->to != 'all') {
			$bills = $bills->whereDate('created_at' , '<='  , $this->to );
		}
		if ($this->driver_id != 'all') {
			$bills = $bills->where('driver_id' , $this->driver_id );
		}
		if ($this->status != 'all') {
			$bills = $bills->where('status' , $this->status );
		}

		$bills = $bills->latest()->get();	

		$pdfContent = PDF::loadView('board.bills.pdf', compact('bills') )->output();
		return response()->streamDownload(
			function()use($pdfContent){
				print($pdfContent);
			},
			"bills.pdf"
		);

	}


	public function excel()
	{

		$bills = Bill::query()->with(['driver'  , 'type' , 'admin' ]);
		if ($this->from != 'all') {
			$bills = $bills->whereDate('created_at' , '>='  , $this->from );
		}
		if ($this->to != 'all') {
			$bills = $bills->whereDate('created_at' , '<='  , $this->to );
		}
		if ($this->driver_id != 'all') {
			$bills = $bills->where('driver_id' , $this->driver_id );
		}
		if ($this->status != 'all') {
			$bills = $bills->where('status' , $this->status );
		}

		$bills = $bills->latest()->get();

		
		return Excel::download(new BillExport($bills), 'bills.xlsx'); 
	}



	protected $paginationTheme = 'bootstrap';
	public function render()
	{
		// dd($this->driver_id);
		$bills = Bill::query()->with(['driver'  , 'type' , 'admin' ]);
		if ($this->from != 'all') {
			$bills = $bills->whereDate('created_at' , '>='  , $this->from );
		}
		if ($this->to != 'all') {
			$bills = $bills->whereDate('created_at' , '<='  , $this->to );
		}
		if ($this->driver_id != 'all') {
			$bills = $bills->where('driver_id' , $this->driver_id );
		}
		if ($this->status != 'all') {
			$bills = $bills->where('status' , $this->status );
		}

		$bills = $bills->latest()->paginate($this->paginate);

		$final = $bills;

		$this->total_accepted_bills_count = $final->where('status' , 1)->count();
		$this->total_refused_bills_count = $final->where('status' , 2)->count();
		$this->total_pending_bills_count = $final->where('status' , 0)->count();

		$this->total_accepted_bills_total_money_amount = $final->where('status' , 1)->sum('price');
		$this->total_refused_bills_total_money_amount = $final->where('status' , 2)->sum('price');
		$this->total_pending_bills_total_money_amount = $final->where('status' , 0)->sum('price');




		$drivers = Driver::all();
		return view('livewire.board.bills' , compact('bills' , 'drivers'));
	}
}
