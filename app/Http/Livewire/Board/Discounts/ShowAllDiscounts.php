<?php

namespace App\Http\Livewire\Board\Discounts;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Discount;
class ShowAllDiscounts extends Component
{


	use WithPagination;


	public $search;
	public $paginate = 10;
	public $active;

	protected $paginationTheme = 'bootstrap';


	public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingPaginate()
    {
        $this->resetPage();
    }



    protected $listeners = ['deleteItemConfirmed' => 'handleItemDeletion'];

    public function handleItemDeletion($item_id)
    {
        Discount::where('id' , $item_id )->delete();
        $this->emit('itemDeleted' , $item_id);
        $this->resetPage();
    }




    public function render()
    {
    	$discounts =  Discount::withCount(['governorates' , 'markets'])->latest()->simplePaginate($this->paginate);
        return view('livewire.board.discounts.show-all-discounts' , compact('discounts'));
    }
}
