<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bill;
use PDF;
class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('board.bills.index');
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $bill)
    {
        $bill->load(['type' , 'admin' , 'driver']);
        return view('board.bills.bill'  , compact('bill') );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print(Bill $bill)
    {
        $bill->load(['type' , 'admin' , 'driver']);
        $pdf = PDF::loadView('board.bills.print',compact('bill') );
        return $pdf->stream('document.pdf')->print();

        // return view('board.bills.print'  , compact('bill') );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function refuse(Bill $bill)
    {
        $bill->status = 2;
        $bill->save();
        return back();
        // return view('board.bills.bill'  , compact('bill') );
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accept(Bill $bill)
    {
        $bill->status = 1;
        $bill->save();
        return back();
    }
   
}
