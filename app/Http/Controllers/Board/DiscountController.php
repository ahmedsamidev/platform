<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerAddress;
use App\Models\Market;
use App\Models\Governorate;
use App\Models\Discount;
use App\Models\DiscountGovernorate;
use App\Models\DiscountMarket;
use Auth;
use App\Http\Requests\Board\Discounts\StoreDiscountRequest;
class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('board.discounts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $governorates = Governorate::all();
        $markets = Market::all();
        $customers = CustomerAddress::all();

        return view('board.discounts.create' , compact('customers', 'markets' , 'governorates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDiscountRequest $request)
    {

        // dd($request->all());
        $discount = new Discount;
        $discount->title = $request->title;
        $discount->admin_id = Auth::guard('admin')->id();
        $discount->starts_at = $request->start_date.' '.$request->start_time;
        $discount->ends_at = $request->end_date.' '.$request->end_time;
        $discount->discount_percentage = $request->percentage;
        $discount->allowed_used_count = $request->allowed_used_count;
        $discount->specific_market = $request->filled('specific_market') ? 1 : 0;
        $discount->specific_area = $request->filled('specific_area') ? 1 : 0;
        $discount->discount_for = $request->discount_for;
        $discount->active = 1;
        $discount->save();


        if($request->filled('markets')) {
            $markets = [];
            foreach ($request->markets as $market) {
                $markets[] = new DiscountMarket([
                    'market_id' => $market , 
                    'discount_id' => $discount->id ,
                ]);
            }
            $discount->markets()->saveMany($markets);
        }


        if($request->filled('governorates')) {
            $governorates = [];
            foreach ($request->governorates as $governorate) {
                $governorates[] = new DiscountGovernorate([
                    'governorate_id' => $governorate , 
                    'discount_id' => $discount->id ,
                ]);
            }
            $discount->governorates()->saveMany($governorates);
        }



        return redirect(route('discounts.index'))->with('success_msg'  , trans('discounts.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        $discount->load(['admin' , 'markets.market' , 'governorates.governorate']);
        return view('board.discounts.show' , compact('discount'));
    }

   

}
