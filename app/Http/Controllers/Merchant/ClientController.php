<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Controller;
use App\MerchantModels\Client;
use App\Models\City;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('merchantDashbaord.customers.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('merchantDashbaord.customers.create',compact('cities'));   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = request()->all();

        $this->validate(request() , [

            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'city_id' => 'required',
        ]);

        $client = new Client($input);
        $client->save();
        
        return redirect(route('clients.index'))->with('success_msg' , trans('merchantDashbaord.added_successfully'));  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        if (empty($client)) {

            return redirect(route('clients.index'))->with('error_msg' , trans('merchantDashbaord.not_foundd'));
        }

        return view('merchantDashbaord.customers.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        $cities = City::all();
        return view('merchantDashbaord.customers.edit',compact('client','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);

        if(empty($client))
        {
            return redirect(route('clients.index'))->with('error_msg' , trans('merchantDashbaord.not_found'));
        }

        $input = request()->all();

        $this->validate(request() , [

            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'city_id' => 'required',
        ]);

        $client->fill($input);
        $client->update();

        return redirect(route('clients.index'))->with('success_msg' , trans('merchantDashbaord.updated_successfully'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);

        if (empty($client)) {

            return redirect(route('clients.index'))->with('error_msg' , trans('merchantDashbaord.not_found'));
        }

        $client->delete();

        return redirect(route('clients.index'))->with('success_msg' , trans('merchantDashbaord.deleted_successfully'));
    }
}
