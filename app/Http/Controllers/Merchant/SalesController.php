<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Controller;
use App\MerchantModels\Client;
use App\MerchantModels\Product;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function showSales()
    {
        $products = Product::orderBy('order','desc')->get();
        $clients = Client::all();
        return view('merchantDashbaord.salesreport.show',compact('products','clients'));
    }
}
