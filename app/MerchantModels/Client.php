<?php

namespace App\MerchantModels;

use App\Models\City;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = 'clients';

    protected $fillable = ['id','name','email','phone','city_id','created_at',          
    'updated_at'];  
    
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
